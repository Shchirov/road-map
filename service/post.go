package service

import (
	"user-service/graph/model"
	"user-service/repository"
)

var _ PostService = PostServiceImpl{}

type PostService interface {
	GetAllPost() ([]*model.Post, error)
}

type PostServiceImpl struct {
	PostRepository repository.PostRepository
}

func NewPostService(postRepository repository.PostRepository) *PostServiceImpl {
	return &PostServiceImpl{PostRepository: postRepository}
}

func (p PostServiceImpl) GetAllPost() ([]*model.Post, error) {

	posts, err := p.PostRepository.FindAllPosts()

	if err != nil {
		return nil, err
	}

	var postsResponse []*model.Post

	for _, post := range posts {
		postsResponse = append(postsResponse, &model.Post{
			ID:    post.Id.String(),
			Title: post.Title,
		})
	}
	return postsResponse, nil
}
