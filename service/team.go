package service

import (
	"user-service/graph/model"
	"user-service/repository"
)

var _ TeamService = TeamServiceImpl{}

type TeamService interface {
	GetAllTeams() ([]*model.Team, error)
}

type TeamServiceImpl struct {
	TeamRepository repository.TeamRepository
}

func NewTeamService(teamRepository repository.TeamRepository) *TeamServiceImpl {
	return &TeamServiceImpl{TeamRepository: teamRepository}
}

func (t TeamServiceImpl) GetAllTeams() ([]*model.Team, error) {
	teams, err := t.TeamRepository.FindAllTeams()
	if err != nil {
		return nil, err
	}

	var teamResponse []*model.Team

	for _, team := range teams {
		teamResponse = append(teamResponse, &model.Team{
			ID:    team.Id.String(),
			Title: team.Title,
		})
	}
	return teamResponse, nil
}
