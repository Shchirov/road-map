package service

import (
	"context"
	"github.com/google/uuid"
	"user-service/entity"
	"user-service/errors"
	"user-service/graph/model"
	"user-service/mapper"
	"user-service/repository"
)

var _ ProjectService = ProjectServiceImpl{}

type ProjectService interface {
	CreateProject(request model.CreateProjectRequest) (*model.Project, error)
	GetProjectById(projectId uuid.UUID) (*model.Project, error)
	GetAllProjects() ([]*model.Project, error)
	GetMembers(id string, ctx context.Context) ([]*model.User, error)
	InviteUserToProject(ctx context.Context, userID *string, projectId *string) (*model.InviteUserResponse, error)
}

type ProjectServiceImpl struct {
	ProjectRepository repository.ProjectRepository
	UserService       UserService
	UserMapper        mapper.UserMapper
}

func NewProjectService(projectRepository repository.ProjectRepository, userService UserService, userMapper mapper.UserMapper) *ProjectServiceImpl {
	return &ProjectServiceImpl{ProjectRepository: projectRepository, UserService: userService, UserMapper: userMapper}
}

func (p ProjectServiceImpl) InviteUserToProject(ctx context.Context, userID *string, projectId *string) (*model.InviteUserResponse, error) {
	authUser, err := p.UserService.GetAuthUser(ctx)

	project, err := p.ProjectRepository.FindProjectById(uuid.MustParse(*projectId))

	if err != nil {
		return nil, err
	}

	if !p.isAdministrator(project.Administrators, authUser) {
		return nil, errors.ForbiddenError{}
	}

	userIdUuid := uuid.MustParse(*userID)
	user, err := p.UserService.GetUserById(&userIdUuid)

	members := project.Members
	members = append(members, user)
	project.Members = members

	_, err = p.ProjectRepository.SaveProject(&project)

	if err != nil {
		return nil, err
	}

	return &model.InviteUserResponse{
		UserID:    userID,
		ProjectID: projectId,
	}, nil
}

func (p ProjectServiceImpl) GetMembers(id string, ctx context.Context) ([]*model.User, error) {
	authUser, err := p.UserService.GetAuthUser(ctx)

	project, err := p.ProjectRepository.FindProjectById(uuid.MustParse(id))

	//todo create custom error
	if err != nil {
		return nil, err
	}

	administrators := project.Administrators

	if !project.IsPublic && !p.isAdministrator(administrators, authUser) {
		return nil, errors.ForbiddenError{}
	}

	members := project.Members
	userModelResponse := p.UserMapper.MapUserModelSlice(members)

	return userModelResponse, nil
}

func (p ProjectServiceImpl) isAdministrator(administrators []*entity.User, authUser *entity.User) bool {
	for _, administrator := range administrators {
		if administrator.Id == authUser.Id {
			return true
		}
	}
	return false
}

func (p ProjectServiceImpl) GetAllProjects() ([]*model.Project, error) {
	projects, err := p.ProjectRepository.FindAll()

	if err != nil {
		return nil, err
	}

	var projectsResponse []*model.Project

	for _, project := range projects {
		projectsResponse = append(projectsResponse, &model.Project{
			ID:       project.Id.String(),
			Title:    project.Title,
			IsPublic: project.IsPublic,
		})
	}
	return projectsResponse, nil
}

func (p ProjectServiceImpl) GetProjectById(projectId uuid.UUID) (*model.Project, error) {
	project, err := p.ProjectRepository.FindProjectById(projectId)
	if err != nil {
		return nil, err
	}
	return &model.Project{
		ID:       project.Id.String(),
		Title:    project.Title,
		IsPublic: project.IsPublic,
	}, nil
}

func (p ProjectServiceImpl) CreateProject(request model.CreateProjectRequest) (*model.Project, error) {

	project := entity.Project{
		Id:             uuid.New(),
		Title:          request.Title,
		IsPublic:       request.IsPublic,
		Members:        []*entity.User{},
		Administrators: []*entity.User{},
	}

	saveProject, err := p.ProjectRepository.SaveProject(&project)
	return &model.Project{
		ID:       saveProject.Id.String(),
		Title:    saveProject.Title,
		IsPublic: saveProject.IsPublic,
	}, err
}
