package service

import (
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"log"
	"time"
	"user-service/config"
	"user-service/entity"
)

//	type Claims struct {
//		Email string `json:"email"`
//		ID    uint   `json:"id"`
//		jwt.StandardClaims
//	}
var _ JwtService = JwtServiceImpl{}

type JwtService interface {
	GenerateToken(user *entity.User) (string, time.Time, error)
	ParseToken(token string) (*jwt.MapClaims, error)
}

type JwtServiceImpl struct {
	Properties *config.AppProperties
}

func NewJwtService(properties *config.AppProperties) *JwtServiceImpl {
	return &JwtServiceImpl{Properties: properties}
}

func (j JwtServiceImpl) GenerateToken(user *entity.User) (string, time.Time, error) {
	exp := time.Now().Add(time.Minute * 10)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user.Email,
		"RegisteredClaims": jwt.RegisteredClaims{
			Subject:   user.Email,
			ExpiresAt: jwt.NewNumericDate(exp),
		},
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString([]byte(j.Properties.SecretToken))

	return tokenString, exp, err
}

func (j JwtServiceImpl) ParseToken(token string) (*jwt.MapClaims, error) {
	var parsedToken, err = jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(j.Properties.SecretToken), nil
	})
	if err != nil {
		log.Println(err)
	}

	if claims, ok := parsedToken.Claims.(jwt.MapClaims); ok {
		return &claims, nil
	} else {
		return nil, err
	}
}
