package service

import (
	"context"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"net/mail"
	"user-service/entity"
	"user-service/errors"
	"user-service/graph/model"
	"user-service/mapper"
	"user-service/repository"
	"user-service/util"
)

var _ UserService = UserServiceImpl{}

type UserService interface {
	LoginUser(email *string, password *string) (*model.Token, error)
	RegisterUser(request model.RegisterUserRequest) (*model.User, error)
	GetUserByEmail(email *string) (*entity.User, error)
	GetAuthUser(ctx context.Context) (*entity.User, error)
	GetUserById(userId *uuid.UUID) (*entity.User, error)
}

type UserServiceImpl struct {
	UserRepository repository.UserRepository
	JwtService     JwtService
	UserMapper     mapper.UserMapper
}

func NewUserService(userRepository repository.UserRepository, jwtService JwtService, userMapper mapper.UserMapper) *UserServiceImpl {
	return &UserServiceImpl{UserRepository: userRepository, JwtService: jwtService, UserMapper: userMapper}
}

func (u UserServiceImpl) GetUserById(userId *uuid.UUID) (*entity.User, error) {
	user := u.UserRepository.FindUserById(*userId)
	return user, nil
}

func (u UserServiceImpl) GetAuthUser(ctx context.Context) (*entity.User, error) {
	claims := ctx.Value("auth").(*jwt.MapClaims)
	email, _ := claims.GetSubject()
	authUser, err := u.GetUserByEmail(&email)

	if err != nil {
		return nil, err
	}
	return authUser, nil
}

func (u UserServiceImpl) GetUserByEmail(email *string) (*entity.User, error) {
	user, err := u.UserRepository.FindUserByEmail(email)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u UserServiceImpl) LoginUser(email *string, password *string) (*model.Token, error) {
	_, err := mail.ParseAddress(*email)
	if err != nil {
		return nil, errors.EmailIsIncorrect{Email: *email}
	}

	user, err := u.UserRepository.FindUserByEmail(email)

	if err != nil || user == nil {
		return nil, errors.UserNotFoundError{Email: *email}
	}

	isPasswordCorrect := util.ComparePassword(*password, user.Password)

	if !isPasswordCorrect {
		return nil, errors.PasswordIsIncorrectError{}
	}

	token, exp, err := u.JwtService.GenerateToken(user)

	if err != nil {
		return nil, err
	}

	expireTime := exp.String()
	return &model.Token{
		Token:    token,
		ExpireAt: &expireTime,
	}, nil

}

func (u UserServiceImpl) RegisterUser(request model.RegisterUserRequest) (*model.User, error) {

	_, err := mail.ParseAddress(request.Email)
	if err != nil {
		return nil, errors.EmailIsIncorrect{Email: request.Email}
	}

	exist, err := u.UserRepository.UserExistByEmail(&request.Email)

	if exist == true || err != nil {
		return nil, errors.UserExistError{Email: request.Email}
	}

	password, err := util.HashPassword(request.Password)

	if err != nil {
		return nil, err
	}

	user := entity.User{
		Id:       uuid.New(),
		Fio:      request.Fio,
		Email:    request.Email,
		Password: password,
		TeamId:   uuid.MustParse(request.TeamID),
		PostId:   uuid.MustParse(request.PostID),
		Team:     entity.Team{Id: uuid.MustParse(request.TeamID)},
		Post:     entity.Post{Id: uuid.MustParse(request.PostID)},
		IsActive: true,
	}

	saveUser, err := u.UserRepository.SaveUser(&user)
	if err != nil {
		return nil, err
	}

	userModelResponse := u.UserMapper.MapUserModel(saveUser)
	return userModelResponse, nil
}
