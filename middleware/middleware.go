package middleware

import (
	"context"
	"github.com/golang-jwt/jwt/v5"
	"net/http"
	"user-service/service"
)

type AuthMiddleware struct {
	JwtService service.JwtService
}

func NewAuthMiddleware(jwtService service.JwtService) *AuthMiddleware {
	return &AuthMiddleware{JwtService: jwtService}
}

func (a AuthMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")

		if auth == "" {
			next.ServeHTTP(w, r)
			return
		}

		bearer := "Bearer "
		auth = auth[len(bearer):]

		claims, err := a.JwtService.ParseToken(auth)

		if err != nil {
			http.Error(w, "Invalid token", http.StatusForbidden)
			return
		}

		ctx := context.WithValue(r.Context(), "auth", claims)

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func CtxValue(ctx context.Context) *jwt.MapClaims {
	raw, _ := ctx.Value("auth").(*jwt.MapClaims)
	return raw
}
