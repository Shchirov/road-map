alter table "road-map".users
    alter column id drop default;

alter table "road-map".project
    alter column id drop default;

alter table "road-map".post
    alter column id drop default;

alter table "road-map".project_administrator
    alter column id drop default;

alter table "road-map".project_members
    alter column id drop default;

alter table "road-map".team
    alter column id drop default;