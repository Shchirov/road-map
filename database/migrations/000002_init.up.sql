create schema "road-map";

create table if not exists "road-map".project
(
    id        uuid primary key,
    title     text not null,
    is_public boolean default true
);

create table if not exists "road-map".team
(
    id         uuid primary key,
    title      text not null,
    is_active  bool      default true,
    updated_dt timestamp default now()
);

create table if not exists "road-map".post
(
    id         uuid primary key,
    title      text not null,
    is_active  bool      default true,
    updated_dt timestamp default now()
);

create table if not exists "road-map".users
(
    id         uuid primary key,
    fio        text not null,
    team_id    uuid references "road-map".team (id),
    post_id    uuid references "road-map".post (id),
    is_active  bool      default true,
    updated_dt timestamp default now()
);

create table if not exists "road-map".project_members
(
    id         uuid primary key,
    project_id uuid references "road-map".project (id),
    user_id    uuid references "road-map".users (id)
);

create table if not exists "road-map".project_administrator
(
    id         uuid primary key,
    project_id uuid references "road-map".project (id),
    user_id    uuid references "road-map".users (id)
);




