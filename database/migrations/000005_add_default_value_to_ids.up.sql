alter table "road-map".users
    alter column id set default gen_random_uuid();

alter table "road-map".project
    alter column id set default gen_random_uuid();

alter table "road-map".post
    alter column id set default gen_random_uuid();

alter table "road-map".project_administrator
    alter column id set default gen_random_uuid();

alter table "road-map".project_members
    alter column id set default gen_random_uuid();

alter table "road-map".team
    alter column id set default gen_random_uuid();