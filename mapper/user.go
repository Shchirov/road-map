package mapper

import (
	"user-service/entity"
	"user-service/graph/model"
)

var _ UserMapper = UserMapperImpl{}

type UserMapper interface {
	MapUserModelSlice(users []*entity.User) []*model.User
	MapUserModel(user *entity.User) *model.User
}

type UserMapperImpl struct {
}

func NewUserMapper() *UserMapperImpl {
	return &UserMapperImpl{}
}

func (u UserMapperImpl) MapUserModel(user *entity.User) *model.User {
	post := user.Post
	team := user.Team
	return &model.User{
		ID:  user.Id.String(),
		Fio: user.Fio,
		Post: &model.Post{
			ID:    post.Id.String(),
			Title: post.Title,
		},
		Team: &model.Team{
			ID:    team.Id.String(),
			Title: team.Title,
		},
	}
}

func (u UserMapperImpl) MapUserModelSlice(users []*entity.User) []*model.User {
	var userModels []*model.User
	for _, user := range users {
		mappedUser := u.MapUserModel(user)
		userModels = append(userModels, mappedUser)
	}
	return userModels
}
