package config

import "github.com/spf13/viper"

type AppProperties struct {
	SecretToken      string `mapstructure:"SECRET_TOKEN"`
	DatabaseName     string `mapstructure:"DATABASE_NAME"`
	DatabaseUsername string `mapstructure:"DATABASE_USERNAME"`
	DatabasePassword string `mapstructure:"DATABASE_PASSWORD"`
	DatabasePort     string `mapstructure:"DATABASE_PORT"`
	DatabaseHost     string `mapstructure:"DATABASE_HOST"`
	GraphQlPort      string `mapstructure:"GRAPHQL_PORT"`
}

func ReadConfig(stand string) *AppProperties {
	var appProperties AppProperties

	viper.AddConfigPath("./config")
	viper.SetConfigName(stand)
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	var err = viper.ReadInConfig()

	err = viper.Unmarshal(&appProperties)
	if err != nil {
		panic(err)
	}
	return &appProperties
}
