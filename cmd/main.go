package main

import (
	"log"
	"user-service/config"
)

func main() {

	configuration := config.ReadConfig("dev")

	db := config.Db(configuration)
	logger := config.NewLogger()

	graphQlApp := NewGraphQlApp(configuration, db, logger)

	err := graphQlApp.Run()
	if err != nil {
		log.Fatal(err)
	}

}
