package main

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"net/http"
	"user-service/config"
	"user-service/directive"
	"user-service/graph"
	"user-service/mapper"
	"user-service/middleware"
	"user-service/repository"
	"user-service/service"
)

var _ App = GraphQlApp{}
var _ App = GrpcApp{}

type BaseApp struct {
	Config *config.AppProperties
	Db     *gorm.DB
	Logger *logrus.Logger
}

type App interface {
	Run() error
}

type GraphQlApp struct {
	BaseApp
}

func NewGraphQlApp(appConfiguration *config.AppProperties, db *gorm.DB, logger *logrus.Logger) *GraphQlApp {
	return &GraphQlApp{BaseApp{
		Config: appConfiguration,
		Db:     db,
		Logger: logger,
	}}
}

func (g GraphQlApp) Run() error {
	postRepository := repository.NewPostRepository(g.Db)
	userRepository := repository.NewUserRepository(g.Db)
	teamRepository := repository.NewTeamRepository(g.Db)
	projectRepository := repository.NewProjectRepository(g.Db)

	userMapper := mapper.NewUserMapper()

	jwtService := service.NewJwtService(g.Config)
	userService := service.NewUserService(userRepository, jwtService, userMapper)
	projectService := service.NewProjectService(projectRepository, userService, userMapper)
	postService := service.NewPostService(postRepository)
	teamService := service.NewTeamService(teamRepository)

	authMiddleware := middleware.NewAuthMiddleware(jwtService)

	router := mux.NewRouter()
	router.Use(authMiddleware.Middleware)

	cfg := graph.Config{Resolvers: &graph.Resolver{
		UserService:    userService,
		PostService:    postService,
		TeamService:    teamService,
		ProjectService: projectService,
	}}
	cfg.Directives.Auth = directive.Auth

	srv := handler.NewDefaultServer(graph.NewExecutableSchema(cfg))

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", srv)

	port := g.Config.GraphQlPort

	err := http.ListenAndServe(":"+port, router)
	if err != nil {
	}
	return nil
}

type GrpcApp struct {
	BaseApp
}

func (g GrpcApp) Run() error {
	//TODO implement me
	panic("implement me")
}
