package errors

import (
	"fmt"
)

var _ error = UserExistError{}

type UserExistError struct {
	Email string
}

func (u UserExistError) Error() string {
	return fmt.Sprintf("user with login = %s already exists", u.Email)
}

type EmailIsIncorrect struct {
	Email string
}

func (e EmailIsIncorrect) Error() string {
	return fmt.Sprintf("email = %s is incorrect", e.Email)
}

type UserNotFoundError struct {
	Email string
}

func (u UserNotFoundError) Error() string {
	return fmt.Sprintf("user with email = %s not found", u.Email)
}

type ForbiddenError struct {
}

func (u ForbiddenError) Error() string {
	return "you are not project administrator"
}
