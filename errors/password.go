package errors

type PasswordIsIncorrectError struct {
}

func (u PasswordIsIncorrectError) Error() string {
	return "password is incorrect"
}
