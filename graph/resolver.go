package graph

import "user-service/service"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your cmd, add any dependencies you require here.

type Resolver struct {
	UserService    service.UserService
	PostService    service.PostService
	TeamService    service.TeamService
	ProjectService service.ProjectService
}
