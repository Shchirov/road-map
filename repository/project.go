package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"user-service/entity"
)

var _ ProjectRepository = ProjectRepositoryImpl{}

type ProjectRepository interface {
	FindAll() ([]entity.Project, error)
	SaveProject(project *entity.Project) (entity.Project, error)
	FindProjectById(projectId uuid.UUID) (entity.Project, error)
}

type ProjectRepositoryImpl struct {
	Db *gorm.DB
}

func (p ProjectRepositoryImpl) FindAll() ([]entity.Project, error) {
	var posts []entity.Project
	err := p.Db.Find(&posts).Error
	return posts, err
}

func NewProjectRepository(db *gorm.DB) *ProjectRepositoryImpl {
	return &ProjectRepositoryImpl{Db: db}
}

func (p ProjectRepositoryImpl) FindProjectById(projectId uuid.UUID) (entity.Project, error) {
	var project entity.Project
	err := p.Db.Preload(clause.Associations).First(&project).Where("id = ?", projectId).Error
	return project, err
}

func (p ProjectRepositoryImpl) SaveProject(project *entity.Project) (entity.Project, error) {
	err := p.Db.Save(&project).Error
	return *project, err
}
