package repository

import (
	"gorm.io/gorm"
	"user-service/entity"
)

var _ TeamRepository = TeamRepositoryImpl{}

type TeamRepository interface {
	FindAllTeams() ([]entity.Team, error)
}

type TeamRepositoryImpl struct {
	Db *gorm.DB
}

func NewTeamRepository(db *gorm.DB) *TeamRepositoryImpl {
	return &TeamRepositoryImpl{Db: db}
}

func (t TeamRepositoryImpl) FindAllTeams() ([]entity.Team, error) {
	var teams []entity.Team
	err := t.Db.Find(&teams).Error

	return teams, err
}
