package repository

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"user-service/entity"
)

var _ UserRepository = UserRepositoryImpl{}

type UserRepository interface {
	FindUserById(id uuid.UUID) *entity.User
	FindUserByEmail(email *string) (*entity.User, error)
	UserExistByEmail(login *string) (bool, error)
	SaveUser(user *entity.User) (*entity.User, error)
}

type UserRepositoryImpl struct {
	Db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepositoryImpl {
	return &UserRepositoryImpl{Db: db}
}

func (u UserRepositoryImpl) SaveUser(user *entity.User) (*entity.User, error) {
	err := u.Db.Save(&user).Error
	return user, err
}

func (u UserRepositoryImpl) UserExistByEmail(email *string) (bool, error) {
	var exists bool
	var err = u.Db.Model(&entity.User{}).
		Select("count(*) > 0").
		Where("email = ?", email).
		Find(&exists).
		Error
	return exists, err
}

func (u UserRepositoryImpl) FindUserByEmail(email *string) (*entity.User, error) {
	var user *entity.User
	err := u.Db.Model(user).Where("email = ?", email).First(&user).Error

	return user, err
}

func (u UserRepositoryImpl) FindUserById(id uuid.UUID) *entity.User {
	//TODO implement me
	panic("implement me")
}
