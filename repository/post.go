package repository

import (
	"gorm.io/gorm"
	"user-service/entity"
)

var _ PostRepository = PostRepositoryImpl{}

type PostRepository interface {
	FindAllPosts() ([]entity.Post, error)
}

type PostRepositoryImpl struct {
	Db *gorm.DB
}

func NewPostRepository(db *gorm.DB) *PostRepositoryImpl {
	return &PostRepositoryImpl{Db: db}

}

func (p PostRepositoryImpl) FindAllPosts() ([]entity.Post, error) {
	var posts []entity.Post
	err := p.Db.Find(&posts).Error
	return posts, err
}
