generate:
	go run github.com/99designs/gqlgen generate

migration-up:
	 migrate -path ./database/migrations -database postgres://root:password@localhost:5432/road_map?sslmode=disable up 1
migration-down:
	 migrate -path ./database/migrations -database postgres://root:password@localhost:5432/road_map?sslmode=disable down 1
migration-create:
	 migrate create -ext sql -dir database/migrations/ -seq $(FILE_NAME)
