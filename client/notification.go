package client

var _ NotificationClient = NotificationClientImpl{}

type NotificationClient interface {
	SendNotification()
}

type NotificationClientImpl struct {
}

func (n NotificationClientImpl) SendNotification() {
	//TODO implement kafka producer to notification topic
	// implements transactional outbox pattern here
	panic("implement me")
}
