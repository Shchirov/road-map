package client

import "github.com/google/uuid"

var _ TaskClient = TaskClientImpl{}

type TaskClient interface {
	GetProjectTasks(projectId uuid.UUID)
}

type TaskClientImpl struct {
}

func (t TaskClientImpl) GetProjectTasks(projectId uuid.UUID) {
	//TODO implement grpc client GetProjectTask method
	panic("implement me")
}
