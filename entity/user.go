package entity

import (
	"github.com/google/uuid"
	"time"
)

type User struct {
	Id        uuid.UUID
	Fio       string
	TeamId    uuid.UUID
	PostId    uuid.UUID
	Password  string
	Email     string
	Team      Team      `gorm:"foreignKey:Id;references:TeamId"`
	Post      Post      `gorm:"foreignKey:Id;references:PostId"`
	IsActive  bool      `gorm:"column:is_active"`
	updatedDt time.Time `gorm:"column:updated_dt"`
}

func (u *User) TableName() string {
	return "road-map.users"
}
