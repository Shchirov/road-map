package entity

import "github.com/google/uuid"

type Project struct {
	Id             uuid.UUID
	Title          string
	IsPublic       bool    `gorm:"column:is_public"`
	Members        []*User `gorm:"many2many:road-map.project_members;"`
	Administrators []*User `gorm:"many2many:road-map.project_administrator;"`
}

func (u *Project) TableName() string {
	return "road-map.project"
}
