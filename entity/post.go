package entity

import (
	"github.com/google/uuid"
	"time"
)

type Post struct {
	Id        uuid.UUID
	Title     string
	IsActive  bool      `gorm:"column:is_active"`
	updatedDt time.Time `gorm:"column:updated_dt"`
}

func (u *Post) TableName() string {
	return "road-map.post"
}
