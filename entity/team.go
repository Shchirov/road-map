package entity

import (
	"github.com/google/uuid"
	"time"
)

type Team struct {
	Id        uuid.UUID
	Title     string
	IsActive  bool      `gorm:"column:is_active"`
	updatedDt time.Time `gorm:"column:updated_dt"`
}

func (u *Team) TableName() string {
	return "road-map.team"
}
